package com.cashfree.io.backend.controller;

import com.cashfree.io.backend.model.User;
import com.cashfree.io.backend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/user")
// change api to v1
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        super();
        this.userService = userService;
    }
    // build create employee REST Api

    @PostMapping()
    public ResponseEntity<User> saveUser(@RequestBody User user){

        return new ResponseEntity<User>(userService.saveUser(user), HttpStatus.CREATED);

    }

    //    build get all employee rest api
    @GetMapping
    public List<User> getAllUser() {

        return userService.getAllUser();
    }


    //    build get employee by ID rest api
//      http://localhost:6799/api/employees/1
    @GetMapping("{id}")
    public ResponseEntity <User> getUserById(@PathVariable("id") long userId) {
        return new ResponseEntity<User>(userService.getUserById(userId),HttpStatus.OK);
    }

    //    build update employee rest api
//    http://localhost:6799/api/employees/1
    @PutMapping("{id}")
    public ResponseEntity <User> updateUser(@PathVariable("id") long id
            ,@RequestBody User user){

        return new ResponseEntity<User>(userService.updateUser(user, id),HttpStatus.OK);
    }

    //    build delete employee rest api
    //    http://localhost:6799/api/employees/1
    @DeleteMapping("{id}")
    public ResponseEntity<String>deleteUser(@PathVariable("id") long id){
//        delete employee by id
        userService.deleteUser(id);
        return new ResponseEntity<String>("Employee deleted successfully..!!",HttpStatus.OK);

    }
}
