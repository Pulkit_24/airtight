package com.cashfree.io.backend.controller;

import com.cashfree.io.backend.model.Tenants;

import com.cashfree.io.backend.service.TenantsService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/tenants")
public class TenantsController {


    private TenantsService tenantsService;

    public TenantsController(TenantsService tenantsService) {
        super();
        this.tenantsService = tenantsService;
    }
    // build create employee REST Api

    @PostMapping()
    public ResponseEntity<Tenants> saveTenants(@RequestBody Tenants tenants){

        return new ResponseEntity<Tenants>(tenantsService.saveTenants(tenants), HttpStatus.CREATED);

    }

    //    build get all employee rest api
    @GetMapping
    public List<Tenants> getAllTenants() {

        return tenantsService.getAllTenants();
    }


    //    build get employee by ID rest api
//      http://localhost:6799/api/employees/1
    @GetMapping("{id}")
    public ResponseEntity <Tenants> getTenantsById(@PathVariable("id") long tenantsId) {
        return new ResponseEntity<Tenants>(tenantsService.getTenantsById(tenantsId),HttpStatus.OK);
    }

    //    build update employee rest api
//    http://localhost:6799/api/employees/1
    @PutMapping("{id}")
    public ResponseEntity <Tenants> updateTenants(@PathVariable("id") long id
            ,@RequestBody Tenants tenants){

        return new ResponseEntity<Tenants>(tenantsService.updateTenants(tenants, id),HttpStatus.OK);
    }

    //    build delete employee rest api
    //    http://localhost:6799/api/employees/1
    @DeleteMapping("{id}")
    public ResponseEntity<String>deleteTenants(@PathVariable("id") long id){
//        delete employee by id
        tenantsService.deleteTenants(id);
        return new ResponseEntity<String>("Employee deleted successfully..!!",HttpStatus.OK);

    }
}


