package com.cashfree.io.backend.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="Full_name",nullable=false)
    private String fullname;

    @Column(name="isactive")
    private long isactive;

    @Column(name="email")
    private String email;

    @Column(name="updated_on")
    private String updatedon;

    @Column(name="updated_by")
    private long updatedby;

}
