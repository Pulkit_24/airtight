package com.cashfree.io.backend.service.impl;

import com.cashfree.io.backend.exception.ResourceNotFoundException;
import com.cashfree.io.backend.model.Tenants;

import com.cashfree.io.backend.repository.TenantsRepository;
import com.cashfree.io.backend.service.TenantsService;
//import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TenantsServiceImpl implements TenantsService {

    private TenantsRepository tenantsRepository;

    public TenantsServiceImpl(TenantsRepository tenantRepository) {
        super();
        this.tenantsRepository = tenantRepository;
    }

    @Override
    public Tenants saveTenants(Tenants tenants) {
        return tenantsRepository.save(tenants);
    }


    @Override
    public List<Tenants> getAllTenants() {
        return tenantsRepository.findAll();
    }

    @Override
    public Tenants getTenantsById(long id) { return tenantsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User","id",id));}



//        Optional<Employee> employee = employeeRepository.findById(id);
//        if(employee.isPresent())
//        {
//            return employee.get();
//        }
//        else
//        {
//            throw new ResourceNotFoundException("Employee","id",id);
//        }
//    }

    @Override
    public Tenants updateTenants(Tenants tenants, long id) {
//        first we need to check whether a employee with given id is present or not
        Tenants existingTenants = tenantsRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Tenants","Id",id));


        existingTenants.setFullname(existingTenants.getFullname());
        existingTenants.setTkey(existingTenants.getTkey());
        existingTenants.setOwnerId(existingTenants.getOwnerId());
        existingTenants.setIsactive(existingTenants.getIsactive());
        existingTenants.setUpdatedon(existingTenants.getUpdatedon());
        existingTenants.setUpdatedby(existingTenants.getUpdatedby());


// save existing employee to db
        tenantsRepository.save(existingTenants);
        return existingTenants;
    }

    @Override
    public void deleteTenants(long id) {

        //        first we need to check whether a employee with given id is present or not
        tenantsRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Tenants","Id",id));

        tenantsRepository.deleteById(id);
    }
}
