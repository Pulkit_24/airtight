package com.cashfree.io.backend.service.impl;

import com.cashfree.io.backend.exception.ResourceNotFoundException;
import com.cashfree.io.backend.model.User;
import com.cashfree.io.backend.repository.UserRepository;
import com.cashfree.io.backend.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(long id) { return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User","id",id));}



//        Optional<Employee> employee = employeeRepository.findById(id);
//        if(employee.isPresent())
//        {
//            return employee.get();
//        }
//        else
//        {
//            throw new ResourceNotFoundException("Employee","id",id);
//        }
//    }

    @Override
    public User updateUser(User user, long id) {
//        first we need to check whether a employee with given id is present or not
        User existingUser = userRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("User","Id",id));

        existingUser.setFullname(existingUser.getFullname());
        existingUser.setIsactive(existingUser.getIsactive());
        existingUser.setEmail(existingUser.getEmail());
        existingUser.setUpdatedon(existingUser.getUpdatedon());
        existingUser.setUpdatedby(existingUser.getUpdatedby());
// save existing employee to db
        userRepository.save(existingUser);
        return existingUser;
    }

    @Override
    public void deleteUser(long id) {

        //        first we need to check whether a employee with given id is present or not
        userRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("User","Id",id));

        userRepository.deleteById(id);
    }
}