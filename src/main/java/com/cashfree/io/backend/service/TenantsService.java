package com.cashfree.io.backend.service;

import com.cashfree.io.backend.model.Tenants;


import java.util.List;

public interface TenantsService {

    Tenants saveTenants(Tenants tenants);
    List<Tenants> getAllTenants();
    Tenants getTenantsById(long id);
    Tenants updateTenants(Tenants tenants,long id);
    void deleteTenants(long id);
}
