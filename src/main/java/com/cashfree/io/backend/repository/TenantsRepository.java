package com.cashfree.io.backend.repository;

import com.cashfree.io.backend.model.Tenants;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantsRepository extends JpaRepository<Tenants,Long> {
}
