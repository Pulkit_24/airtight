package com.cashfree.io.backend.repository;

import com.cashfree.io.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
